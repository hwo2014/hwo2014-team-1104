#ifndef TRACKMEMORY_H_
#define TRACKMEMORY_H_

#include <ostream>

class trackPiece;

class trackMemory
{
public:
  trackMemory(trackPiece * track_piece, int lane, double entering_speed);

  int          getLane()          const;
  trackPiece * getTrackPiece()    const;
  bool         isCrashed()        const;
  double       getEnteringSpeed() const;
  double       getCrashingSpeed() const;
  double       getMaxCarAngle()   const;
  void         dump(std::ostream & output) const;

  void crash(double speed);
  void updateMaxCarAngle(double angle);

private:
  trackPiece * _track_piece;
  int          _lane;
  double       _entering_speed;
  double       _max_car_angle;
  bool         _crashed;
  double       _crashing_speed;
};


#endif
