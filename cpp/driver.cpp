#include "brakingMemory.h"
#include "car.h"
#include "carSlidingMemory.h"
#include "driver.h"
#include "race.h"
#include "settings.h"
#include "track.h"
#include "trackMemory.h"
#include "trackPiece.h"
#include "trackPieceBend.h"
#include <cassert>
#include <cmath>
#include <iostream>
#include <set>
#include <utility>

driver::driver()
  : _min_braking(-1.0),
    _min_braking_speed(0.0),
    _max_braking(-1.0),
    _max_braking_speed(0.0)
{
}


#define MAX_MEM_SIZE 1024

void driver::memorizeTrack(trackMemory * memory_piece)
{
  if(settings::globals->getBool("debugTrackMemories"))
    memory_piece->dump(settings::globals->log());
  if(this->_track_memories.size() < MAX_MEM_SIZE)
    this->_track_memories.push_back(memory_piece);
}


void driver::memorizeCarSliding(carSlidingMemory * memory_piece)
{
  if(this->_car_sliding_memories.size() < MAX_MEM_SIZE)
    this->_car_sliding_memories.push_back(memory_piece);
}


void driver::memorizeBraking(brakingMemory * memory_piece)
{
  if(memory_piece->getThrottle() < 0.1)
    {
      if(this->_braking_memories.size() < MAX_MEM_SIZE)
        this->_braking_memories.push_back(memory_piece);
      
      double sd = memory_piece->getSpeedDifference();

      if(this->_min_braking < 0.0 || sd < this->_min_braking)
        {
          this->_min_braking = sd;
          this->_min_braking_speed = memory_piece->getStartingSpeed();
        }

      if(this->_max_braking < 0.0 || sd > this->_max_braking)
        {
          this->_max_braking = sd;
          this->_max_braking_speed = memory_piece->getStartingSpeed();
        }
    }
}


int driver::getOptimalLane(car * mycar, track * track, const race & race) const
{
  /* Determine the optimum lane by looking from the next track piece until two lane changes are possible,
   * calculate the number of insidelanes for left/right and use the one with most turning.
   */

  /* todo: perhaps don't do any lineswitching while turbo is on, or favor outer lanes? */

  double total_angles[2];
  trackPiece * next_switch = NULL;

  total_angles[0] = 0.0;
  total_angles[1] = 0.0;


  int piece_index = mycar->getCurrentTrackPiece()->getIndex() + 1;
  for(int switches = 0; switches < 2; piece_index++)
    {
      trackPiece * p = track->getPiece(piece_index);
      if(p->isStraight() == false)
        {
          int side = p->getInsideLane() == 0 ? 0 : 1; /* clamp side to 0 or 1 */
          total_angles[side] += std::abs(p->getAngle());
        }
      if(p->hasSwitch())
        {
          switches++;
          if(next_switch == NULL)
            next_switch = p;
        }
    }

  int lane;

  if(total_angles[0] > total_angles[1])
    lane = 0;
  else
    lane = track->getLaneCount() - 1;

  settings::globals->log() << "  lane: optimal=" << lane << ", between track pieces " << (1 + mycar->getCurrentTrackPiece()->getIndex() + 1) << " and " << (1 + piece_index - 1);
  settings::globals->log() << ", total_angles=" << total_angles[0] << "/" << total_angles[1];

  
  /* If close to a switch, and car is on a high angle, take the lane that would lessen the cars angle.
   * Don't change lane if the switch piece is either straight or turning to the "easening" direction.
   */
  assert(next_switch != NULL);

  double car_angle = mycar->getAngle();
  settings::globals->log() << ", car_angle=" << car_angle;
  if(std::abs(car_angle) > 15.0)
    {
      settings::globals->log() << ":over15";
      if(mycar->isAngleIncreasing())
        {
          settings::globals->log() << ":increasing";
          if(track->getPiece(next_switch->getIndex() - 1) == mycar->getCurrentTrackPiece())
            {
              settings::globals->log() << ":next=switch:distance=" << mycar->getDistanceToTrackPiece(next_switch->getIndex());
              if(next_switch->isStraight() == false)
                if((car_angle > 0.0 && next_switch->getAngle() > 0.0) ||
                   (car_angle < 0.0 && next_switch->getAngle() < 0.0)   )
                  {
                    settings::globals->log() << ":switch_not_easening";
                    
                    if(mycar->getDistanceToTrackPiece(next_switch->getIndex()) < mycar->getSpeed() * 2.0)
                      {
                        if(car_angle > 0.0)
                          lane = 0;
                        else
                          lane = track->getLaneCount() - 1;
                        settings::globals->log() << ":force the use of lane " << lane;
                      }
                  }
            }
        }
    }

  if(track->getPiece(mycar->getCurrentTrackPiece()->getIndex() + 1) == next_switch)
    {
      int tick = mycar->getTick();

      { /* If we have recently collided on the car ahead of us, switch lane to overtake it. */
        int lastcollision = mycar->getLastCollisionFront();
        settings::globals->log() << ", overtake{tick=" << tick << ":lastCollision=" << lastcollision;
        if(lastcollision >= 0 && tick - lastcollision <= settings::globals->getInt("overtakeTickTime"))
          {
            car * other = race.getNextCar(mycar);
            if(other != NULL)
              {
                settings::globals->log() << ":other=" << other->getColour();
                if(other->getCurrentTrackPiece() == next_switch)
                  {
                    settings::globals->log() << ":atSwitch";
                    if(other->getEndLane() == lane)
                      {
                        settings::globals->log() << ":lanesEqual";
                        if(lane == 0)
                          lane = 1;
                        else
                          lane -= 1;
                      }
                  }
              }
          }
        settings::globals->log() << "}";
      }
    }

  settings::globals->log() << " -> lane=" << lane << std::endl;

  return lane;
}



double driver::getOptimalThrottle(car * mycar, track * track, const race & race) const
{
  double throttle = 1.0;

  /* todo: if in race, first, and there were no crashes in previous and this lap, then don't try to adjust 
   *       also check that the second car is not getting nearer
   */

  /* todo: During qualifying, make sure the last lap does not have crashes. */

  /* todo: calculate if it's worth to not to break and crash instead at full speed
   *       instead of calculating, maybe one lap to test this?
   */

  /* Lookahead
   * 1. get target speed for this and next pieces
   * 2. if there's enough time to slow down for the next piece, then accelerate, else brake
   * 3. do more than one piece lookahead
   */
  for(int lookahead = 0; throttle > 0.0 && lookahead < settings::globals->getInt("lookahead"); lookahead++)
    {
      settings::globals->log() << "  lookahead-" << lookahead << "=";

      trackPiece * tpiece  = track->getPiece(mycar->getCurrentTrackPiece()->getIndex() + lookahead);

      if(tpiece->isStraight())
        settings::globals->log() << "s";
      else
        settings::globals->log() << "b";
      settings::globals->log() << ":";


      double target_speed  = this->getMaxEnteringSpeed(mycar, track, race, tpiece->getIndex());
      double current_speed = mycar->getSpeed();
      
      if(lookahead == 0)
        { /* Calculate score for angle -alarm. */
          double score = 0.0;

          double a_score = std::pow(std::abs(mycar->getAngle()) / settings::globals->getDouble("curveLimitAngle"), 2);
          score += a_score;

          double ac_score = std::pow(std::abs(mycar->getAngleChange()) / settings::globals->getDouble("curveLimitAngleChange"), 2);
          score += ac_score;

          double acd_score = std::pow(std::abs(mycar->getAngleChangeDerivative()) / settings::globals->getDouble("curveLimitAngleChangeDerivative"), 2);
          score += acd_score;


          settings::globals->log() << " cla:{"
                                   << "(angle="        << std::abs(mycar->getAngle())                 << "/" << settings::globals->getDouble("curveLimitAngle")                 << "=" << a_score
                                   << ",angleChange="  << mycar->getAngleChange()           << "/" << settings::globals->getDouble("curveLimitAngleChange")           << "=" << ac_score
                                   << ",angleChangeD=" << std::abs(mycar->getAngleChangeDerivative()) << "/" << settings::globals->getDouble("curveLimitAngleChangeDerivative") << "=" << acd_score
                                   << ")=score=" << score;

          if(score >= settings::globals->getDouble("curveLimitScore"))
            { /* If the cars angle is increasing alarmingly, lower the speed until the cars angle stays stable or decreases. */
              double speed = this->getNoSlidingSpeed(tpiece, mycar->getLane());
              settings::globals->log() << ",corneringSpeed=" << speed;

              if(speed > 0.0)
                if(target_speed <= 0.0 || target_speed > speed)
                  {
                    target_speed = speed;
                    settings::globals->log() << "->target_speed=" << target_speed;
                  }
              /* Occasionally getNoSlidingSpeed() might return higher speed than our current speed, make sure we throttle down: */
              if(target_speed > current_speed * 0.95)
                target_speed = current_speed * 0.95;
            }
          settings::globals->log() << "} ";
        }

      /* Default driving only for the current track piece. */
      if(target_speed <= 0.0)
        if(lookahead == 0)
          if(tpiece->isStraight() == false)
            {
              settings::globals->log() << "default";
              target_speed = this->getNoSlidingSpeed(tpiece, mycar->getLane());
            }

      if(target_speed > 0.0)
        if(current_speed > target_speed)
          {
            double tmp = this->brakeTo(mycar, tpiece->getIndex(), target_speed);
            if(tmp < throttle)
              throttle = tmp;
          }

      settings::globals->log() << " -> throttle=" << throttle << std::endl;
    }

  return throttle;
}

/* Returns speed suitable for going thru the current curve while reduce sliding. */
double driver::getNoSlidingSpeed(trackPiece * track_piece, int lane) const
{
  double minspeed = -1;

  settings::globals->log() << " noSlidingSpeed:{";

  if(track_piece->isStraight())
    {
      minspeed = 1.0;
      settings::globals->log() << " straight";
    }
  else
    {
      assert(std::abs(track_piece->getAngle()) > 0.0);

      /* Get the minimum speed when car has started to increase its angle in a similar bend, and subtract a small value from it. */
      settings::globals->log() << " angle=" << std::abs(track_piece->getAngle()) << " lane=" << lane;

      minspeed = this->getSlidingStartingSpeed(track_piece, lane);
    }
  
  if(minspeed > 0.0)
    { /* If the we're going faster than the minspeed, then brake. */
      minspeed *= settings::globals->getDouble("curveLimitMultiplier");
      settings::globals->log() << " speed=" << minspeed;
    }
      
  settings::globals->log() << " }";

  return minspeed;
}



double driver::getMaxEnteringSpeed(car * mycar, track * track, const race & race, int piece_index) const
{ /* todo: calculate this when information comes in, or maybe store in a binary tree */
  trackPiece * tp = track->getPiece(piece_index);

  settings::globals->log() << " maxEntSpeed(";

  double min_crashed_speed = -1.0;
  double max_success_speed = -1.0;
  double max_success_angle = 0.0;
  double lowest_crashing_speed = -1.0;

  bool found = false;

  for(unsigned int i = 0; i < this->_track_memories.size(); i++)
    {
      trackMemory * tm = this->_track_memories[i];

      if(tm->getTrackPiece()->getIndex() == tp->getIndex())
        { /* The track piece and memory match, obtain data from the memory. */
          found = true;
          if(tm->isCrashed())
            {
              double spd = tm->getEnteringSpeed();
              if(min_crashed_speed < 0.0 || spd < min_crashed_speed)
                min_crashed_speed = spd;
            }
          else
            {
              double spd = tm->getEnteringSpeed();
              if(max_success_speed < spd)
                {
                  max_success_speed = spd;
                  max_success_angle = tm->getMaxCarAngle();
                }
            }
        }

      /* Update lowest_crashing_speed */
      if(tm->isCrashed())
        {
          double speed = std::min(tm->getEnteringSpeed(), tm->getCrashingSpeed());
              
          if(lowest_crashing_speed < 0.0 || speed < lowest_crashing_speed)
            lowest_crashing_speed = speed;
        }
    }

  double target_speed = -1.0;

  int lane = mycar->getLane(); /* todo: forecast the lane at the given track piece */

  if(found)
    {
      settings::globals->log() << ", max_success_speed=" << max_success_speed << ", min_crashed_speed=" << min_crashed_speed << ",";
      min_crashed_speed *= settings::globals->getDouble("minCrashSpeedMultiplier");
      if(max_success_speed > 0.0)
        { /* There were successes.
           * If there were failures too, take the average of those two, otherwise use the (only existing) success speed.
           */

          /* If the maximum success had low car angle, add to its speed a bit, to try harder next time. */
          /* todo: don't use msa_add if the race is on, and we have been first for one lap, and nobody is gaining on us
           *       or easier version: if the race is on, and we have been first for this and previous lap, and nobody gained on us in previous lap
           */
          double msa_limit = settings::globals->getDouble("maxSuccessAngleLimit");
          max_success_angle = std::abs(max_success_angle);
          settings::globals->log() << "msa=" << max_success_angle << "/" << msa_limit;

          double msa_add = 0.0;
          if(max_success_angle < msa_limit)
            {
              double multiplier = settings::globals->getDouble("maxSuccessAngleAdd");
              if(race.isQualifying() == false)
                multiplier *= 0.75;

              assert(multiplier >= 1.0);

              msa_add = (msa_limit - max_success_angle) / msa_limit * multiplier;
            }

          if(min_crashed_speed > 0.0)
            {
              settings::globals->log() << " using=min-max-average,";
              if(max_success_speed + msa_add < min_crashed_speed)
                {
                  settings::globals->log() << " msa_add=" << msa_add << ",";
                  target_speed = (max_success_speed + msa_add + min_crashed_speed) / 2.0;
                }
              else
                target_speed = (max_success_speed + min_crashed_speed) / 2.0;
            }
          else
            {
              settings::globals->log() << " msa_add=" << msa_add << ",";
              settings::globals->log() << " using=max_success,";
              target_speed = max_success_speed + msa_add;

              if(tp->isStraight() == false)
                {
                  /* Need to be careful here, since we have not crashed we do not know our upper limit. */
                  target_speed = this->limitCorneringSpeed(target_speed, tp, lane);
                }
            }
        }
      else if(min_crashed_speed > 0.0)
        {
          settings::globals->log() << " using=min-crash,";
          target_speed = min_crashed_speed;
        }
    }
  else
    {
      /* todo: If no matching track memory was found, try to construct the required information from other ones. */
      if(tp->isStraight())
        {
          target_speed = 999.0;
        }
      else
        {
          target_speed = this->limitCorneringSpeed(999.0, tp, lane);
        }
    }

  settings::globals->log() << " -> target_speed=" << target_speed << " )";

  return target_speed;
}


double driver::limitCorneringSpeed(double target_speed, trackPiece * track_piece, int lane) const
{
  /* Don't go so fast to start sliding horribly. */
  double noslidespeed = this->getNoSlidingSpeed(track_piece, lane);
  /* Add some to allow some sliding... */
  noslidespeed *= settings::globals->getDouble("slidingAllowanceMultiplier");
  assert(noslidespeed > 0.0);

  if(noslidespeed < target_speed)
    {
      settings::globals->log() << "{limitCorneringSpeed}";
      target_speed = noslidespeed;
    }

  return target_speed;
}

double driver::brakeTo(car * mycar, int piece_index, double target_speed) const
{
  double throttle = 1.0;

  settings::globals->log() << " brakeTo:{";
  
  if(mycar->getSpeed() > target_speed)
    {
      double distance_to_travel = mycar->getDistanceToTrackPiece(piece_index);
      double current_speed      = mycar->getSpeed();
      
      settings::globals->log() << " distToTravel=" << distance_to_travel << ", curSpeed=" << current_speed;

      while(distance_to_travel > 0.0 && current_speed > target_speed)
        {
          double braking_amount = this->getBrakingAmount(current_speed);
          distance_to_travel -= current_speed;
          current_speed      -= braking_amount;
        }
      
      settings::globals->log() << " => distToTravel=" << distance_to_travel << ", curSpeed=" << current_speed;

      if(current_speed > target_speed)
        { /* It is not possible to brake in time. Try to brake anyway. */
          throttle = 0.0;
          settings::globals->log() << " UNABLE TO BRAKE ENOUGH (trying anyway)";
        }
      else
        { /* It is possible to break in time.
           * If the distance to travel left is close to the first braking amount, then we need to start braking.
           */
          settings::globals->log() << " ABLE TO BRAKE ENOUGH";
          if(distance_to_travel <= this->getBrakingAmount(mycar->getSpeed()) * settings::globals->getDouble("brakingAheadTicks"))
            {
              throttle = 0.0;
              settings::globals->log() << " (will try to brake)";
            }
          else
            {
              settings::globals->log() << " (don't need to brake yet)";
            }
        }
    }

  settings::globals->log() << " => " << throttle << " }";

  return throttle;
}


/* Return the amount of braking done by one tick from the given speed, if throttle is set to 0. */
double driver::getBrakingAmount(double speed) const
{
  double braking_amount = 0.1;

  if(this->_braking_memories.size() > 1)
    {
      /* Observed that braking is more efficient at lower speeds.
       * But there are also other factors.
       * Only proceed if we have "sane" values.
       */
      if(this->_min_braking_speed >= this->_max_braking_speed)
        { /* Take the current speed, clamp it between the speed range of memories, and use it to get approximate braking per tick value. */
      
          /* Clamp the speed between 0..(min_braking_speed - max_braking_speed). */
          if(speed < this->_max_braking_speed)
            speed = this->_max_braking_speed;
          else if(speed > this->_min_braking_speed)
            speed = this->_min_braking_speed;
          
          speed -= this->_max_braking_speed;
          assert(speed >= 0.0);
          assert(speed <= this->_min_braking_speed - this->_max_braking_speed);
          
          /* Turn speed into a percentage. */
          speed /= this->_min_braking_speed - this->_max_braking_speed;
          
          /* And use it to get the braking amount */
          braking_amount = this->_min_braking + speed * (this->_max_braking * this->_min_braking);
        }
    }

  return braking_amount;
}


void driver::dumpTrackMemories(std::ostream & output) const
{
  output << "Dumping track memories:" << std::endl;
  for(unsigned int i = 0; i < this->_track_memories.size(); i++)
    this->_track_memories[i]->dump(output);
}



void driver::dumpSlidingStartingSpeeds(std::ostream & output) const
{
  output << "Dumping sliding starting speeds:" << std::endl;

  output << "Dumping sliding starting speeds is not working currently." << std::endl;

#if 0
  /* todo: need to obtain the real lane info instead of using the tmplanes below, pass track* in here,
   * and instead of using lane info to build temporary track pieces, use the track to obtain the trackpieces
   */
  std::set<std::pair<double, double> *> angles;

  for(unsigned int i = 0; i < this->_car_sliding_memories.size(); i++)
    {
      double angle  = std::abs(this->_car_sliding_memories[i]->getTrackPiece()->getAngle());
      double radius = this->_car_sliding_memories[i]->getTrackPiece()->getRadius();
      angles.insert(new std::pair<double, double>(angle, radius));
    }

  for(std::set<std::pair<double, double> *>::iterator it = angles.begin(); it != angles.end(); it++)
    {
      std::vector<double> tmplanes = { 0 };
      trackPieceBend p(0, tmplanes, (*it)->first, (*it)->second, false);
      for(int lane = 0; lane < 2; lane++)
        output << " angle="  << (*it)->first
               << " radius=" << (*it)->second
               << " lane="   << lane
               << " speed="  << this->getSlidingStartingSpeed(&p, lane)
               << std::endl;
    }
#endif
}


double driver::getSlidingStartingSpeed(trackPiece * track_piece, int lane) const
{
  double minspeed  = -1.0;
  double tp_angle  = std::abs(track_piece->getAngle());
  double tp_length = track_piece->getLength(lane);

  for(unsigned int i = 0; i < this->_car_sliding_memories.size(); i++)
    {
      carSlidingMemory * m = this->_car_sliding_memories[i];
      if(m->getCarAngleChange() > 0.0)
        {
          double m_angle = std::abs(m->getTrackPiece()->getAngle());
          double m_length = m->getTrackPiece()->getLength(lane);
          if(std::abs(m_angle - tp_angle) < 10.0)
            if(std::abs(m_length - tp_length) < 10.0)
              if(minspeed < 0.0 || m->getSpeed() < minspeed)
                minspeed = m->getSpeed();
        }
    }
  if(minspeed < 0.0)
    minspeed = 3.5;

  return minspeed;
}


bool driver::getUseTurbo(car * mycar, track * track) const
{
  bool turbo_on = false;

  if(settings::globals->getBool("turboEnabled"))
    {
      /* todo: if someone going same speed or slower is on same track and we would reach him before next switch, don't use turbo yet */
      /* todo: instead of testing only the current angle of the car, get a score for all three angle values (angle, change, change derivative), and base decision on those */
      settings::globals->log() << "  turbo:"
                               << " available"
                               << " carThrottle=" << mycar->getThrottle() << "/" << settings::globals->getDouble("turboMinThrottle")
                               << " carAngle="    << mycar->getAngle()    << "/" << settings::globals->getDouble("turboMaxCarAngle")
        ;
      if(mycar->isTurboAvailable())
        if(mycar->getThrottle() >= settings::globals->getDouble("turboMinThrottle"))
          if(std::abs(mycar->getAngle()) <= settings::globals->getDouble("turboMaxCarAngle"))
            if(track->getPiece(mycar->getCurrentTrackPiece()->getIndex() + 1)->isStraight())
              if(track->getPiece(mycar->getCurrentTrackPiece()->getIndex() + 2)->isStraight() ||
                 track->getNextCurveAngle(mycar->getCurrentTrackPiece()->getIndex()) < settings::globals->getDouble("turboMaxCurveAngle"))
                turbo_on = true;
      
      settings::globals->log() << " -> turbo=" << (turbo_on ? "true" : "false") << std::endl;
    }

  return turbo_on;
}

