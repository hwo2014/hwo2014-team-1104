#include "car.h"
#include "race.h"
#include "settings.h"
#include "trackPiece.h"
#include <cassert>
#include <iostream>

race::race()
  : _track(NULL),
    _session_laps(0),
    _session_max_lap_time(0),
    _my_car(NULL),
    _running(false),
    _qualifying(true)
{
}


void race::start()
{
  settings::globals->log() << "RACE START" << std::endl;
  this->_running = true;
  
  for(auto car : this->_cars)
    car->beginRace();
}

void race::end()
{
  settings::globals->log() << "RACE END" << std::endl;
  this->_running = false;
  this->_qualifying = false;
  this->getMyCar()->endRace();

  for(unsigned int i = 0; i < this->getMyCar()->getLapCount(); i++)
    this->getMyCar()->outputLapInfo(i);
}

bool race::isRunning() const
{
  return this->_running;
}


bool race::isQualifying() const
{
  return this->_qualifying;
}


void race::setTrack(track * track)
{
  this->_track = track;
}

void race::setMyCarColour(const std::string & colour)
{
  this->_my_car_colour = colour;
}

void race::lapFinish(const std::string & car_colour, unsigned int ticks, unsigned int milliseconds)
{
  car * c = this->getCarByColour(car_colour);
  if(c != NULL)
    c->lapFinish(ticks, milliseconds);
}

car * race::getCarByColour(const std::string & colour) const
{
  car * rv = NULL;

  for(unsigned i = 0; rv == NULL && i < this->_cars.size(); i++)
    if(this->_cars[i]->getColour() == colour)
      rv = this->_cars[i];

  return rv;
}


void race::carCrashed(const std::string & car_colour)
{
  car * c = this->getCarByColour(car_colour);
  if(c != NULL)
    {
      c->crash();
      if(c == this->getMyCar())
        {
          trackPiece * p = c->getCurrentTrackPiece();

          settings::globals->log() << "CRASH!";
          settings::globals->log() << " track=";
          if(p->isStraight())
            settings::globals->log() << "straight";
          else
            settings::globals->log() << "bend(angle=" << p->getAngle() << ")";
          settings::globals->log() << ": waiting for spawn" << std::endl;
        }
    }
}

void race::carCrashRecovery(const std::string & car_colour)
{
  car * c = this->getCarByColour(car_colour);
  if(c != NULL)
    {
      c->crashRecovery();
      if(c == this->getMyCar())
        settings::globals->log() << "Back on track!" << std::endl;
    }
}


car * race::getMyCar() const
{
  assert(this->_my_car != NULL);

  return this->_my_car;
}


const std::string & race::getMyCarColour() const
{
  //return this->getMyCar()->getColour();
  return this->_my_car_colour;
}



const track & race::getTrack() const
{
  assert(this->_track != NULL);

  return *this->_track;
}


void race::addCar(const std::string & car_colour, double length, double width, double guide_flag_position)
{
  assert(this->_track != NULL);

  car * c = new car(this->_track, car_colour, length, width, guide_flag_position);
  this->_cars.push_back(c);

  if(this->_my_car == NULL)
    if(car_colour == this->_my_car_colour)
      this->_my_car = c;
}


unsigned int race::getCarCount() const
{
  return this->_cars.size();
}



void race::updateCarPosition(const std::string & car_colour, double angle, int pieceIndex, double pieceDistance, int startLaneIndex, int endLaneIndex)
{
  car * c = NULL;
  for(unsigned int i = 0; c == NULL && i < this->_cars.size(); i++)
    if(this->_cars[i]->getColour() == car_colour)
      c = this->_cars[i];

  assert(c != NULL);
  assert(startLaneIndex >= 0);
  assert(endLaneIndex >= 0);

  c->updatePosition(angle, pieceIndex, pieceDistance, startLaneIndex, endLaneIndex);
}


void race::checkForCollisions()
{
  if(this->_running)
    for(unsigned int i = 0; i < this->_cars.size(); i++)
      for(unsigned int j = i + 1; j < this->_cars.size(); j++)
        {
          car * car1 = this->_cars[i];
          car * car2 = this->_cars[j];
          car1->checkCollision(car2);
        }
}


void race::setSessionLaps(unsigned int laps)
{
  this->_session_laps = laps;
}


unsigned int race::getSessionLaps() const
{
  return this->_session_laps;
}


void race::setSessionMaxLapTime(unsigned int milliseconds)
{
  this->_session_max_lap_time = milliseconds;
}


unsigned int race::getSessionMaxLapTime() const
{
  return this->_session_max_lap_time;
}



car * race::getNextCar(const car * of_who) const
{
  car *  closest_car      = NULL;
  double closest_distance = 0.0;

  for(auto other : this->_cars)
    if(other != of_who)
      if(other->isCrashed() == false)
        {
          double d = of_who->getDistanceToTrackPiece(other->getCurrentTrackPiece()->getIndex(), other->getInPieceDistance());
          if(closest_car == NULL || d < closest_distance)
            {
              closest_car      = other;
              closest_distance = d;
            }
        }

  return closest_car;
}


void race::turboAvailable(unsigned int duration_ticks, double factor)
{
  for(auto c : this->_cars)
    c->setTurbo(duration_ticks, factor);
}
