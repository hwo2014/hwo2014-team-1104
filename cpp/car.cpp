#include "brakingMemory.h"
#include "car.h"
#include "carSlidingMemory.h"
#include "driver.h"
#include "lapInfo.h"
#include "settings.h"
#include "track.h"
#include "trackMemory.h"
#include "trackPiece.h"
#include <cassert>
#include <iostream>

car::car(track * track, const std::string & colour, double length, double width, double guide_flag_position)
  : _driver(NULL),
    _track(track),
    _laps(NULL),
    _colour(colour),
    _length(length),
    _width(width),
    _guide_flag_position(guide_flag_position),
    _crashed(false),
    _angle(0.0),
    _angle_change(0.0),
    _piece_index(0),
    _piece_distance(0.0),
    _start_lane_index(0),
    _end_lane_index(0),
    _current_track_memory(NULL),
    _throttle(1.0),
    _throttle_min(0.0)
{
}


const std::string & car::getColour() const
{
  return this->_colour;
}


double car::getLength() const
{
  return this->_length;
}


void car::setDriver(driver * driver)
{
  this->_driver = driver;
}

void car::beginRace()
{
  this->_current_track_memory = NULL;
  this->_send_lane_switch = true;
  this->_crashed     = false;
  this->_crash_count = 0;
  this->_target_lane = -99;
  this->_turbo_duration = 0;
  this->_turbo_factor   = 0.0;
  this->_turbo_use      = false;
  delete this->_laps;
  this->_laps = new std::vector<lapInfo *>();

  this->_tick = 0;
  this->_last_collision_front = -1;
  this->_last_collision_rear  = -1;
}

void car::endRace()
{
  if(this->_current_track_memory != NULL)
    this->_driver->memorizeTrack(this->_current_track_memory);
}

void car::lapFinish(unsigned int ticks, unsigned int milliseconds)
{
  lapInfo * i = new lapInfo(ticks, milliseconds, this->_crash_count);
  this->_laps->push_back(i);

  this->_crash_count = 0;

  if(this->_driver != NULL)
    {
      settings::globals->log() << "LAP " << this->_laps->size() << " FINISHED: ticks=" << i->getTicks() << ", milliseconds=" << i->getMilliseconds() << ", crashes=" << i->getCrashes() << std::endl;
      std::cout << "LAP " << this->_laps->size() << " FINISHED: ticks=" << i->getTicks() << ", milliseconds=" << i->getMilliseconds() << ", crashes=" << i->getCrashes() << "         " << std::endl;
    }
}


void car::crash()
{
  this->_crashed = true;
  this->_crash_count++;
  if(this->_throttle_min > 0.1)
    this->_throttle_min -= 0.1;

  this->_turbo_duration = 0;
  this->_turbo_factor   = 0.0;

  if(this->_driver != NULL)
    {
      std::cout << "  CRASHED at " << this->getPositionString() << "                          " << std::endl;
      if(this->_current_track_memory != NULL)
        this->_current_track_memory->crash(this->_speed);
    }
}


const std::string & car::getPositionString() const
{
  static std::string str;

  double p = 100.0 * this->_piece_distance / this->getCurrentTrackPiece()->getLength(this->_start_lane_index);
  str = std::to_string(p) + "% into track piece #" + std::to_string(this->getCurrentTrackPiece()->getIndex() + 1) + " on lap " + std::to_string(this->getLapCount() + 1);

  return str;
}

void car::crashRecovery()
{
  this->_crashed = false;
}


bool car::isCrashed() const
{
  return this->_crashed;
}

unsigned int car::getCrashCount() const
{
  return this->_crash_count;
}


void car::setThrottle(double throttle)
{
  if(throttle < this->_throttle_min)
    throttle = this->_throttle_min;
  else if(throttle > 1.0)
    throttle = 1.0;

  this->_throttle = throttle;
}


double car::getThrottle() const
{
  return this->_throttle;
}


void car::updatePosition(double angle, int pieceIndex, double pieceDistance, int startLaneIndex, int endLaneIndex)
{
  this->_tick++;

  if(this->_tick > 1)
    {
      double previous_speed = this->_speed;

      if(this->_piece_index == pieceIndex)
        {
          this->_speed = pieceDistance - this->_piece_distance;
        }
      else
        { /* NOTE: This assumes that the car moves only one piece a time. */
          trackPiece * prev = this->_track->getPiece(this->_piece_index);

          /* Average between start and end lanes to hopefully get close enough length. */
          double prevlength = (prev->getLength(this->_start_lane_index) + prev->getLength(this->_end_lane_index)) / 2.0;
          if(this->_start_lane_index != this->_end_lane_index)
            { /* If lane switching occured, add the distance between the two lanes. */
              prevlength += this->_track->getDistanceBetweenLanes(this->_start_lane_index, this->_end_lane_index);
            }

          assert(this->_piece_distance <= prevlength);

          double speed = pieceDistance + prevlength - this->_piece_distance;

          if(this->_crashed || std::abs(speed - this->_speed) < 2.0)
            { /* Use the newly calculated speed if it's withing range of the previous, otherwise use the previous speed value. */
              this->_speed = speed;
            }
          else if(settings::globals->getBool("debugSpeedCalcErrors"))
            {
              settings::globals->log() << "SPEED ERROR: from " << this->_speed << " to " << speed << ":";
              if(this->_start_lane_index != this->_end_lane_index)
                settings::globals->log() << " there was a line switch";
              else
                settings::globals->log() << " no line switch";

              if(prev->isStraight())
                settings::globals->log() << ", straight";
              else
                settings::globals->log() << ", bend(" << prev->getAngle() << ")";

              settings::globals->log() << std::endl;
            }
        }
      assert(this->_speed >= 0.0);
      if(this->_speed < 0.0)
        this->_speed = 0.0;

      if(this->_driver != NULL)
        {
          if(this->_crashed == false)
            if(this->_speed < previous_speed)
              { /* We're slowing down, memorize this braking. */
                if(settings::globals->getBool("debugBrakingMemories"))
                  settings::globals->log() << "BRAKE: from_speed=" << previous_speed << ", to_speed=" << this->_speed << ", throttle=" << this->_throttle << ", car_angle=" << this->_angle << std::endl;

                brakingMemory * m = new brakingMemory(previous_speed, this->_speed, this->_throttle);
                this->_driver->memorizeBraking(m);
              }

          if(this->_piece_index != pieceIndex)
            { /* Entered next piece. */
              if(this->_current_track_memory != NULL)
                this->_driver->memorizeTrack(this->_current_track_memory);

              this->_current_track_memory = new trackMemory(this->_track->getPiece(pieceIndex), startLaneIndex, this->_speed);
            }
          else
            { /* Still on same piece. */
              if(this->_current_track_memory != NULL)
                this->_current_track_memory->updateMaxCarAngle(angle);
            }
        }


      if(this->_driver != NULL)
        {
          if((this->_angle <= 0 && angle <= 0 && angle < this->_angle) ||
             (this->_angle >= 0 && angle >= 0 && angle > this->_angle)   )
            {
              if(this->_angle_increasing == false)
                {
                  carSlidingMemory * mem = new carSlidingMemory(this->_angle, angle, this->_speed, this->_track->getPiece(pieceIndex));
                  this->_driver->memorizeCarSliding(mem);
                }
          
              if(settings::globals->getBool("debugCarSliding"))
                settings::globals->log() << "started_now="              << (this->_angle_increasing == false ? "true" : "false")
                                         << "angle_increase car_angle=" << this->_angle
                                         << " car_angle_new="           << angle
                                         << " car_speed="               << this->_speed
                                         << " car_throttle="            << this->_throttle
                                         << " track_angle="             << this->_track->getPiece(pieceIndex)->getAngle()
                                         << " track_radius="            << this->_track->getPiece(pieceIndex)->getRadius()
                                         << std::endl;

              this->_angle_increasing = true;
            }
          else
            this->_angle_increasing = false;
        }

      if(this->_piece_index != pieceIndex)
        if(this->_track->getPiece(pieceIndex)->hasSwitch())
          this->_send_lane_switch = true;

      this->_angle_change_derivative = this->_angle_change - (angle - this->_angle);
      this->_angle_change            = angle - this->_angle;
    }
  else
    {
      this->_speed                   = 0.0;
      this->_angle_increasing        = false;
      this->_angle_change_derivative = 0.0;
      this->_angle_change            = 0.0;
    }

  this->_angle            = angle;
  this->_piece_index      = pieceIndex;
  this->_piece_distance   = pieceDistance;
  this->_start_lane_index = startLaneIndex;
  this->_end_lane_index   = endLaneIndex;

  if(this->_target_lane == -99)
    { /* First time we know our current lane. */
      this->_target_lane      = this->_end_lane_index;
    }
}


void car::checkCollision(car * other)
{
  if(this->_start_lane_index == other->_start_lane_index)
    if(this->_end_lane_index == other->_end_lane_index)
      {
        double distance;

        distance = this->getDistanceToTrackPiece(other->getCurrentTrackPiece()->getIndex(), other->getInPieceDistance());
        if(distance <= this->getLength())
          { /* this collides into the other */
            this->onCollisionToAhead(other);
            other->onCollisionFromBehind(this);
          }
        else
          {
            distance = other->getDistanceToTrackPiece(this->getCurrentTrackPiece()->getIndex(), this->getInPieceDistance());
            if(distance <= other->getLength())
              { /* the other collides into this */
                other->onCollisionToAhead(this);
                this->onCollisionFromBehind(other);
              }
          }
      }
}

void car::onCollisionFromBehind(const car * other)
{
  settings::globals->log() << "Car " << this->_colour << " was collided from behind by " << other->getColour() << std::endl;

  this->_last_collision_rear = this->_tick;

  delete this->_current_track_memory;
  this->_current_track_memory = NULL;
}

void car::onCollisionToAhead(const car * other)
{
  settings::globals->log() << "Car " << this->_colour << " collided on " << other->getColour() << std::endl;

  this->_last_collision_front = this->_tick;

  delete this->_current_track_memory;
  this->_current_track_memory = NULL;
}



void car::setTurbo(unsigned int duration_ticks, double factor)
{
  if(this->isCrashed() == false)
    {
      if(this->_driver != NULL)
        settings::globals->log() << "Turbo: duration=" << duration_ticks << ", factor=" << factor << std::endl;
      this->_turbo_duration = duration_ticks;
      this->_turbo_factor   = factor;
    }
}

bool car::getUseTurbo() const
{
  bool use = this->_turbo_use;

  if(this->_crashed)
    use = false;

  return use;
}

void car::useTurbo()
{
  assert(this->_turbo_duration > 0.0);
  assert(this->_crashed == false);
  settings::globals->log() << "Turbo activated." << std::endl;
  this->_turbo_duration = 0;
}

bool car::isTurboAvailable() const
{
  return this->_turbo_duration > 0;
}

double car::getTurboFactor() const
{
  return this->_turbo_factor;
}



double car::getSpeed() const
{
  return this->_speed;
}

void car::updateLane(const race & race)
{
  this->_target_lane = this->_driver->getOptimalLane(this, this->_track, race);
}


int car::getEndLane() const
{
  return this->_end_lane_index;
}



void car::updateUseTurbo()
{
  this->_turbo_use = false;

  if(this->_driver != NULL)
    if(this->_crashed == false)
      if(this->_turbo_duration > 0)
        this->_turbo_use = this->_driver->getUseTurbo(this, this->_track);
}


void car::updateThrottle(const race & race)
{
  if(this->isCrashed() == false)
    this->setThrottle(this->_driver->getOptimalThrottle(this, this->_track, race));
}


int car::getLaneSwitch()
{
  int lane_switch = 0;

  if(this->_send_lane_switch)
    {
      if(this->_target_lane < this->_end_lane_index)
        lane_switch = -1; // "Right";
      else if(this->_target_lane > this->_end_lane_index)
        lane_switch = 1; // "Left";

      if(lane_switch != 0)
        this->_send_lane_switch = false;
    }

  return lane_switch;
}




trackPiece * car::getCurrentTrackPiece() const
{
  trackPiece * current = this->_track->getPiece(this->_piece_index);

  return current;
}


double car::getInPieceDistance() const
{
  return this->_piece_distance;
}





double car::getAngle() const
{
  return this->_angle;
}


double car::getAngleChange() const
{
  return this->_angle_change;
}

double car::getAngleChangeDerivative() const
{
  return this->_angle_change_derivative;
}

int car::getLane() const
{
  return this->_start_lane_index;
}



double car::getDistanceToTrackPiece(int piece_index) const
{
  double distance = 0.0;

  int current = this->_piece_index;
  if(current != piece_index)
    {
      trackPiece * piece;
      int lane;

      /* todo: Determine the correct lane for each track piece.
       *       For now we assume that we are going to stay on the current lane, which is wrong.
       *       Emulate the lane switching logic, it's needed elsewhere too, so make a method in driver to get the lane for a given track piece (in the future)
       */
      lane = this->_target_lane;
      if(lane < 0) /* Target lane not yet set, this happens after qualifying, when race starts. */
        lane = this->_end_lane_index;


      /* Start with the remaining of the current piece. */
      piece = this->_track->getPiece(current);
      distance += piece->getLength(lane) - this->_piece_distance;

      /* Add the rest of the pieces, until we hit the target piece. */
      while(piece->getIndex() != piece_index)
        {
          current++;
          piece = this->_track->getPiece(current);
          if(piece->getIndex() != piece_index)
            distance += piece->getLength(lane);
        }
    }

  
  return distance;
}

double car::getDistanceToTrackPiece(int piece_index, double in_piece_distance) const
{
  double distance = this->getDistanceToTrackPiece(piece_index);

  if(this->_piece_index != piece_index)
    distance += in_piece_distance;

  return distance;
}



bool car::isAngleIncreasing() const
{
  return this->_angle_increasing;
}


unsigned int car::getLapCount() const
{
  assert(this->_laps != NULL);
  return this->_laps->size();
}


bool car::outputLapInfo(unsigned int lap) const
{
  bool found = false;

  if(lap < this->_laps->size())
    {
      lapInfo * i = this->_laps->at(lap);

      found = true;
      settings::globals->log() << "Lap #" << (lap + 1) << " statistics for " << this->_colour << ": ticks=" << i->getTicks() << ", milliseconds=" << i->getMilliseconds() << ", crashes=" << i->getCrashes() << std::endl;
    }

  return found;
}

void car::dump(std::ostream & output) const
{
  output << "Dumping car info:"                                    << std::endl;
  output << "  length="              << this->_length              << std::endl;
  output << "  width="               << this->_width               << std::endl;
  output << "  guide_flag_position=" << this->_guide_flag_position << std::endl;
}



int car::getTick() const
{
  return this->_tick;
}


int car::getLastCollisionFront() const
{
  return this->_last_collision_front;
}


int car::getLastCollisionBack() const
{
  return this->_last_collision_rear;
}

