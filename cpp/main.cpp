#include <ctime>
#include <iostream>
#include <fstream>
#include <string>
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include <jsoncons/json.hpp>
#pragma GCC diagnostic pop
#include "protocol.h"
#include "connection.h"
#include "game_logic.h"
#include "settings.h"

using namespace hwo_protocol;

static std::string track_name = "";

void run(hwo_connection& connection, const std::string& name, const std::string& key)
{
  game_logic game;

  if(track_name == "")
    {
      connection.send_requests({ make_join(name, key) });
    }
  else
    {
      jsoncons::json data;
      jsoncons::json botId;
      
      botId["name"] = (*settings::globals->getString("driverName"));
      botId["key"]  = key;
      data["botId"] = botId;
      
      data["trackName"] = track_name;
      
      data["password"] = "y543gbj02abc892vndfv.,m.,q3mjirfoaj--kljsdzjzfroifjq3or4";

      data["carCount"] = settings::globals->getInt("driverCount");
      if(settings::globals->getBool("driverDummy"))
        connection.send_requests({ make_request("joinRace", data) });
      else
        connection.send_requests({ make_request("createRace", data) });
    }

  for (;;)
  {
    boost::system::error_code error;
    auto response = connection.receive_response(error);

    if (error == boost::asio::error::eof)
    {
      settings::globals->log() << "Connection closed" << std::endl;
      break;
    }
    else if (error)
    {
      throw boost::system::system_error(error);
    }

    std::vector<jsoncons::json> message = game.react(response);
    if(message.size() > 0)
      connection.send_requests(message);
  }
}

int main(int argc, const char* argv[])
{
  int rv;
  std::ostream * log_file = NULL;

  if(argc != 5 && argc != 6)
    {
      std::cerr << "Usage: ./run host port botname botkey" << std::endl;
      return 1;
    }

  try
  {
    const std::string host(argv[1]);
    const std::string port(argv[2]);
    const std::string name(argv[3]);
    const std::string key(argv[4]);

    std::ifstream * sfile;

    sfile = new std::ifstream("settings.json");
    if(sfile->is_open())
      {
        settings::globals = new settings(*sfile);
        delete sfile;
      }
    else
      std::cerr << "ERROR: settings.json could not be opened" << std::endl;

    assert(settings::globals != NULL);

    if(argc == 6)
      track_name = argv[5];
    else if(settings::globals->getString("trackName") != NULL)
      track_name = *(settings::globals->getString("trackName"));
    else
      track_name = "keimola";


    sfile = new std::ifstream("debugging");
    if(sfile->is_open())
      {
        delete sfile;

        char timestamp[1024];
        std::time_t t = std::time(NULL);
        std::tm * tm = std::localtime(&t);
        std::strftime(timestamp, sizeof timestamp, "%Y-%m-%d_%H:%M:%S.txt", tm);
        
        std::string filename = "log-" + track_name + "-" + (*settings::globals->getString("driverName")) + "-" + timestamp;
        std::cout << "Opening log file: " << filename << std::endl;
        log_file = new std::ofstream(filename);
      }
    else
      {
        std::cout << "NOT opening log file." << std::endl;
        log_file = new std::ofstream("/dev/null");
        track_name = "";
      }
    settings::globals->setLogFile(*log_file);

    settings::globals->log() << argv[0] << " was built on " << __DATE__ << " " << __TIME__ << std::endl;
    settings::globals->dump();

    settings::globals->log() << "Host: " << host << ", port: " << port << ", name: " << name << ", key:" << key << std::endl;

    hwo_connection connection(host, port);
    run(connection, name, key);

    rv = 0;
  }
  catch (const std::exception& e)
  {
    std::cerr << e.what() << std::endl;
    rv = 2;
  }

  delete log_file;

  return rv;
}
