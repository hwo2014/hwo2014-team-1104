#ifndef TRACKPIECEBEND_H_
#define TRACKPIECEBEND_H_

#include "trackPiece.h"

#include <vector>

class trackPieceBend : public trackPiece
{
public:
  trackPieceBend(int index, const std::vector<double> & lanes, double angle, double radius, bool has_switch);

  bool   isStraight()        const override;
  int    getInsideLane()     const override;
  double getLength(int lane) const override;
  double getAngle()          const override;
  double getRadius()         const override;

private:
  double              _angle;
  double              _radius;
  std::vector<double> _lengths;
};

#endif
