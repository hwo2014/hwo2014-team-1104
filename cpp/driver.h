#ifndef DRIVER_H_
#define DRIVER_H_

#include <vector>

class brakingMemory;
class car;
class carSlidingMemory;
class race;
class track;
class trackMemory;
class trackPiece;

class driver
{
public:
  driver();

  void memorizeBraking(brakingMemory * memory_piece);
  void memorizeCarSliding(carSlidingMemory * memory_piece);
  void memorizeTrack(trackMemory * memory_piece);
  void dumpSlidingStartingSpeeds(std::ostream & output) const;
  void dumpTrackMemories(std::ostream & output) const;

  int    getOptimalLane(car * mycar, track * track, const race & race)     const;
  bool   getUseTurbo(car * mycar, track * track)                           const;
  double getOptimalThrottle(car * mycar, track * track, const race & race) const;

private:
  double getMaxEnteringSpeed(car * mycar, track * track, const race & race, int piece_index)   const;
  double brakeTo(car * mycar, int piece_index, double target_speed)         const;
  double getBrakingAmount(double speed)                                     const;
  double getSlidingStartingSpeed(trackPiece * track_piece, int lane)        const;
  double getNoSlidingSpeed(trackPiece * track_piece, int lane)              const; /* Returns speed suitable for going thru the current curve to reduce sliding. */
  double limitCorneringSpeed(double target_speed, trackPiece * track_piece, int lane) const;

  double                       _car_length;
  double                       _car_width;
  double                       _car_guide_flag_position;

  std::vector<trackMemory *>      _track_memories;
  std::vector<brakingMemory *>    _braking_memories;
  std::vector<carSlidingMemory *> _car_sliding_memories;

  double _min_braking;       /* minimum braking amount per tick */
  double _min_braking_speed; /* the speed at the minimum braking amount per tick */
  
  double _max_braking;
  double _max_braking_speed;
};

#endif
