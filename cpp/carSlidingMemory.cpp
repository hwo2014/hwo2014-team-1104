#include "carSlidingMemory.h"
#include <cmath>

carSlidingMemory::carSlidingMemory(double car_angle1, double car_angle2, double speed, trackPiece * track_piece)
  : _car_angle_change(std::abs(car_angle1 - car_angle2)),
    _speed(speed),
    _track_piece(track_piece)
{
}


double carSlidingMemory::getCarAngleChange() const
{
  return this->_car_angle_change;
}


const trackPiece * carSlidingMemory::getTrackPiece() const
{
  return this->_track_piece;
}


double carSlidingMemory::getSpeed() const
{
  return this->_speed;
}
