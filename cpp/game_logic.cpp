#include "car.h"
#include "driver.h"
#include "game_logic.h"
#include "protocol.h"
#include "settings.h"
#include "track.h"
#include "trackPiece.h"
#include <string>
#include <vector>
#include <cassert>

using namespace hwo_protocol;

game_logic::game_logic()
  : action_map
    {
      { "carPositions",   &game_logic::on_car_positions   },
      { "crash",          &game_logic::on_crash           },
      { "createRace",     &game_logic::on_create_race     },
      { "error",          &game_logic::on_error           },
      { "gameEnd",        &game_logic::on_game_end        },
      { "gameInit",       &game_logic::on_game_init       },
      { "gameStart",      &game_logic::on_game_start      },
      { "join",           &game_logic::on_join            },
      { "lapFinished",    &game_logic::on_lap_finished    },
      { "spawn",          &game_logic::on_spawn           },
      { "turboAvailable", &game_logic::on_turbo_available },
      { "yourCar",        &game_logic::on_your_car        }
    },
    _driver(NULL)
{
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
  const auto& msg_type = msg["msgType"].as<std::string>();
  const auto& data = msg["data"];
  auto action_it = action_map.find(msg_type);
  if (action_it != action_map.end())
  {
    if(settings::globals->getBool("debugMessageType"))
      settings::globals->log() << "IN: " << msg_type << std::endl;
    if(settings::globals->getBool("debugMessageData"))
      settings::globals->log() << data << std::endl;
    return (action_it->second)(this, data);
  }
  else
  {
    settings::globals->log() << "Unknown message type: " << msg_type << std::endl;
    settings::globals->log() << data << std::endl;
    return { };
  }
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data __attribute__((unused)))
{
  return { };
}

game_logic::msg_vector game_logic::on_create_race(const jsoncons::json& data)
{
  return this->on_join(data);
}


game_logic::msg_vector game_logic::on_your_car(const jsoncons::json & data)
{
  this->_race.setMyCarColour(data["color"].as<std::string>());

  return { };
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json & data)
{
  settings::globals->log() << "TRACK DATA:";

  { /* Track */
    const jsoncons::json & datatrack  = data["race"]["track"];

    track * trk = new track(datatrack["id"].as<std::string>());
    settings::globals->log() << " name=" << trk->getName();

    /* Lanes */
    const jsoncons::json & datalanes  = datatrack["lanes"];

    for(unsigned int i = 0; i < datalanes.size(); i++)
      {
        /* Find the correct laneind, just in case they come unordered. */
        int laneind = -1;

        for(unsigned int j = 0; laneind == -1 && j < datalanes.size(); j++)
          if(datalanes[j]["index"].as<int>() == static_cast<int>(i))
            laneind = j;
        assert(laneind != -1);

        trk->addLane(datalanes[laneind]["distanceFromCenter"].as<double>());
      }
    settings::globals->log() << " lanes=" << trk->getLaneCount();


    /* Pieces */
    const jsoncons::json & datapieces = datatrack["pieces"];

    for(unsigned int i = 0; i < datapieces.size(); i++)
      {
        bool has_switch = false;

        if(datapieces[i].has_member("switch"))
          has_switch = datapieces[i]["switch"].as<bool>();

        if(datapieces[i].has_member("length"))
          { /* straight */
            double length = datapieces[i]["length"].as<double>();
            trk->addPiece(length, has_switch);
          }
        else
          { /* bend */
            double angle  = datapieces[i]["angle"].as<double>();
            double radius = datapieces[i]["radius"].as<double>();
            trk->addPiece(angle, radius, has_switch);
          }
      }
    settings::globals->log() << " pieces=" << trk->getPieceCount();



    this->_race.setTrack(trk);
  }

  /* Cars */
  const jsoncons::json & datacars = data["race"]["cars"];
  
  for(unsigned int i = 0; i < datacars.size(); i++)
    this->_race.addCar(datacars[i]["id"]["color"].as<std::string>(),
                       datacars[i]["dimensions"]["length"].as<double>(),
                       datacars[i]["dimensions"]["width"].as<double>(),
                       datacars[i]["dimensions"]["guideFlagPosition"].as<double>()
                       );

  settings::globals->log() << " cars=" << this->_race.getCarCount();


  /* Race session */
  const jsoncons::json & datasession = data["race"]["raceSession"];
  
  if(datasession.has_member("laps"))
    {
      this->_race.setSessionLaps(datasession["laps"].as<int>());
      this->_race.setSessionMaxLapTime(datasession["maxLapTimeMs"].as<int>());
      settings::globals->log() << " laps=" << this->_race.getSessionLaps();
      settings::globals->log() << " maxLapTime=" << this->_race.getSessionMaxLapTime();
    }

  settings::globals->log() << std::endl;


  this->_race.getMyCar()->dump(settings::globals->log());

  if(this->_driver == NULL)
    {
      this->_driver = new driver();
      this->_race.getMyCar()->setDriver(this->_driver);
    }

  return { };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data __attribute__((unused)))
{
  this->_race.start();

  return { make_throttle(1.0) };
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
  /* Update all cars. */
  for(unsigned int i = 0; i < data.size(); i++)
    this->_race.updateCarPosition(data[i]["id"]["color"].as<std::string>(),
                                  data[i]["angle"].as<double>(),
                                  data[i]["piecePosition"]["pieceIndex"].as<int>(),
                                  data[i]["piecePosition"]["inPieceDistance"].as<double>(),
                                  data[i]["piecePosition"]["lane"]["startLaneIndex"].as<int>(),
                                  data[i]["piecePosition"]["lane"]["endLaneIndex"].as<int>());
  this->_race.checkForCollisions();

  game_logic::msg_vector rv;

  if(this->_race.isRunning())
    {
      /* Figure out what to do. */
      car * mycar = this->_race.getMyCar();
      assert(mycar != NULL);

      if(mycar->isCrashed() == false)
        {
          settings::globals->log() << "Lap=" << (mycar->getLapCount() + 1) << "(" << (mycar->getCurrentTrackPiece()->getIndex() + 1) << "/" << this->_race.getTrack().getPieceCount() << ") speed=" << mycar->getSpeed() << ", throttle=" << mycar->getThrottle() << ", angle=" << mycar->getAngle() << ":" << std::endl;

          std::cout << "Lap=" << (mycar->getLapCount() + 1) << "(" << (mycar->getCurrentTrackPiece()->getIndex() + 1) << "/" << this->_race.getTrack().getPieceCount() << ") speed=" << mycar->getSpeed() << ", throttle=" << mycar->getThrottle() << ", angle=" << mycar->getAngle() << "                    \r" << std::flush;
          
          mycar->updateLane(this->_race);
          mycar->updateThrottle(this->_race);
          mycar->updateUseTurbo();
        }


      /* Do it. */
      int direction = mycar->getLaneSwitch();

      if(direction)
        {
          std::string str = direction < 0 ? "Left" : "Right";
          rv = { make_request("switchLane", str) };
        }
      else if(mycar->getUseTurbo())
        {
          std::cout << "  TURBO at " << mycar->getPositionString() << "                             " << std::endl;
          mycar->useTurbo();
          rv = { make_request("turbo", "Nyt kaahataan!") };
        }
      else
        {
          /* todo: when crashed, throttle at full already to not to miss the first tick back on track */
          rv = { make_throttle(mycar->getThrottle()) };
        }
    }
  else
    {
      rv = { make_throttle(1.0) };
    }

  return rv;
}


game_logic::msg_vector game_logic::on_lap_finished(const jsoncons::json& data)
{
  this->_race.lapFinish(data["car"]["color"].as<std::string>(),
                        data["lapTime"]["ticks"].as<int>(),
                        data["lapTime"]["millis"].as<int>());

  return { };
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
  this->_race.carCrashed(data["color"].as<std::string>());

  return { };
}

game_logic::msg_vector game_logic::on_spawn(const jsoncons::json& data)
{
  this->_race.carCrashRecovery(data["color"].as<std::string>());

  return { };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data __attribute__((unused)))
{
  this->_race.end();
  
  this->_driver->dumpTrackMemories(settings::globals->log());
  this->_driver->dumpSlidingStartingSpeeds(settings::globals->log());

  return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
  settings::globals->log() << "Error: " << data.to_string() << std::endl;
  return { };
}

game_logic::msg_vector game_logic::on_turbo_available(const jsoncons::json& data)
{
  int    ticks  = 1;
  double factor = 2.0;

  if(data.has_member("turboDurationTicks"))
    ticks = data["turboDurationTicks"].as<int>();
  else
    std::cout << "turboAvailable: has no member turboDurationTicks: " << data << std::endl;

  if(data.has_member("turboFactor"))
    factor = data["turboFactor"].as<double>();
  else
    std::cout << "turboAvailable: has no member turboFactor: " << data << std::endl;

  this->_race.turboAvailable(ticks, factor);

  return { };
}
