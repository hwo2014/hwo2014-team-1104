#ifndef TRACKPIECE_H_
#define TRACKPIECE_H_

class trackPiece
{
public:
  trackPiece(int index, bool has_switch);

  int            getIndex() const;
  bool           hasSwitch() const;

  virtual bool   isStraight()        const = 0;
  virtual int    getInsideLane()     const = 0;
  virtual double getLength(int lane) const = 0;
  virtual double getAngle()          const = 0;
  virtual double getRadius()         const = 0;
  
protected:
  int  _index;
  bool _has_switch;
};


#endif
