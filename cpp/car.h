#ifndef CAR_H_
#define CAR_H_

#include <ostream>
#include <string>
#include <vector>

class driver;
class lapInfo;
class race;
class track;
class trackMemory;
class trackPiece;

class car
{
public:
  car(track * track, const std::string & colour, double length, double width, double guide_flag_position);

  const std::string & getColour()                 const;
  double              getLength()                 const;
  void                dump(std::ostream & output) const;

  void setDriver(driver * driver);
  void beginRace();
  void endRace();
  void lapFinish(unsigned int ticks, unsigned int milliseconds);

  unsigned int        getLapCount()                   const;
  const std::string & getPositionString()             const;
  bool                outputLapInfo(unsigned int lap) const;

  void   updatePosition(double angle, int pieceIndex, double pieceDistance, int startLaneIndex, int endLaneIndex);
  double getAngle()                 const;
  double getAngleChange()           const;
  double getAngleChangeDerivative() const;
  bool   isAngleIncreasing()        const;
  int    getLane()                  const;
  int    getEndLane()               const;

  int    getTick()               const;
  int    getLastCollisionFront() const;
  int    getLastCollisionBack()  const;
  void   checkCollision(car * other);
  void   onCollisionFromBehind(const car * other);
  void   onCollisionToAhead(const car * other);

  void   setTurbo(unsigned int duration_ticks, double factor);
  bool   isTurboAvailable()  const;
  double getTurboFactor()    const;
  void   updateUseTurbo();
  bool   getUseTurbo()       const; /* Return true if the car should use the turbo. */
  void   useTurbo();

  trackPiece * getCurrentTrackPiece() const;
  double       getInPieceDistance()   const;
  double       getDistanceToTrackPiece(int piece_index) const;
  double       getDistanceToTrackPiece(int piece_index, double in_piece_distance) const;

  void         crash();
  void         crashRecovery();
  bool         isCrashed()     const;
  unsigned int getCrashCount() const;

  void   updateThrottle(const race & race);
  double getThrottle() const;
  void   setThrottle(double throttle);

  void updateLane(const race & race);
  int  getLaneSwitch();

  double getSpeed() const;

private:
  driver *                 _driver;
  track *                  _track;
  std::vector<lapInfo *> * _laps;
  std::string              _colour;
  double                   _length;
  double                   _width;
  double                   _guide_flag_position;

  bool        _crashed;
  int         _crash_count;

  double      _angle;
  double      _angle_change;
  double      _angle_change_derivative;
  bool        _angle_increasing;
  int         _piece_index;
  double      _piece_distance;
  int         _start_lane_index;
  int         _end_lane_index;
  double      _speed;

  unsigned int _turbo_duration;
  double       _turbo_factor;
  bool         _turbo_use;

  trackMemory * _current_track_memory;

  double      _throttle;
  double      _throttle_min;

  int         _target_lane;
  bool        _send_lane_switch;

  int         _tick;
  int         _last_collision_front;
  int         _last_collision_rear;
};

#endif
