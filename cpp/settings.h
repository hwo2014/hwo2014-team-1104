#ifndef SETTINGS_H_
#define SETTINGS_H_

#include <string>
#include <iostream>
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include <jsoncons/json.hpp>
#pragma GCC diagnostic pop

class settings
{
public:
  settings(std::istream & input);

  static settings * globals;

  void           dump() const;
  void           setLogFile(std::ostream & log_file);
  std::ostream & log();

  double        getDouble(const std::string & name) const;
  std::string * getString(const std::string & name) const;
  bool          getBool(const std::string & name)   const;
  int           getInt(const std::string & name)    const;

private:
  std::ostream * _log_file;
  jsoncons::json _data;
};

#endif
