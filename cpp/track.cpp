#include "track.h"
#include "trackPieceBend.h"
#include "trackPieceStraight.h"
#include <cassert>
#include <cmath>

track::track(std::string name)
  : _name(name)
{
}


const std::string & track::getName() const
{
  return this->_name;
}


void track::addPiece(double length, bool has_switch)
{
  trackPiece * piece = new trackPieceStraight(this->_pieces.size(), length, has_switch);
  this->_pieces.push_back(piece);
}

void track::addPiece(double angle, double radius, bool has_switch)
{
  trackPiece * piece = new trackPieceBend(this->_pieces.size(), this->_lane_distances, angle, radius, has_switch);
  this->_pieces.push_back(piece);
}

trackPiece * track::getPiece(int index) const
{
  assert(index > static_cast<int>(this->_pieces.size() * -2));
  assert(index < static_cast<int>(this->_pieces.size() * 2));

  if(index < 0)
    index += this->_pieces.size();
  else if(index >= static_cast<int>(this->_pieces.size()))
    index -= this->_pieces.size();

  return this->_pieces[index];
}



void track::addLane(double distanceFromCenter)
{
  this->_lane_distances.push_back(distanceFromCenter);
}


int track::getNextInsideLane(int pieceIndex) const
{
  int lane = -1;

  while(lane == -1)
    {
      trackPiece * p = this->getPiece(pieceIndex);
      if(p->isStraight() == false)
        {
          if(p->getAngle() < 0.0)
            {
              lane = 0;
            }
          else
            {
              lane = this->_lane_distances.size() - 1;
            }
        }
      else
        pieceIndex++;
    }

  return lane;
}


double track::getDistanceBetweenLanes(int lane1, int lane2)
{
  double d1, d2, distance;

  d1 = this->_lane_distances[lane1];
  d2 = this->_lane_distances[lane2];
  distance = std::abs(d2 - d1);
  
  return distance;
}

unsigned int track::getPieceCount() const
{
  return this->_pieces.size();
}

unsigned int track::getLaneCount() const
{
  return this->_lane_distances.size();
}


double track::getNextCurveAngle(int piece_index) const
{
  double angle = 0.0;
  int angledir = 0;

  /* Find the start of the curve. */
  while(angledir == 0)
    {
      trackPiece * p = this->getPiece(piece_index);
      if(p->isStraight() == false)
        {
          angle    = std::abs(p->getAngle());
          angledir = p->getAngle();
        }
      piece_index++;
    }
  
  /* Go through the curve until the angle direction changes or a straight emerges. */
  bool done = false;
  while(done == false)
    {
      trackPiece * p = this->getPiece(piece_index);
      if(p->isStraight())
        {
          done = true;
        }
      else if((p->getAngle() < 0.0 && angledir < 0) ||
              (p->getAngle() > 0.0 && angledir > 0)   )
        {
          angle += std::abs(p->getAngle());
        }
      else
        {
          done = true;
        }
      piece_index++;
    }

  return angle;
}
