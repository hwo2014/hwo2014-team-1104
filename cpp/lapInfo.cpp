#include "lapInfo.h"

lapInfo::lapInfo(unsigned int ticks, unsigned int milliseconds, unsigned int crashes)
  : _ticks(ticks),
    _milliseconds(milliseconds),
    _crashes(crashes)
{
}


unsigned int lapInfo::getTicks() const
{
  return this->_ticks;
}


unsigned int lapInfo::getMilliseconds() const
{
  return this->_milliseconds;
}


unsigned int lapInfo::getCrashes() const
{
  return this->_crashes;
}

