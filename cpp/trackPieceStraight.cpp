#include "trackPieceStraight.h"
#include <cassert>

trackPieceStraight::trackPieceStraight(int index, double length, bool has_switch)
  : trackPiece(index, has_switch)
{
  this->_length = length;
}

bool trackPieceStraight::isStraight() const
{
  return true;
}

double trackPieceStraight::getLength(int lane __attribute__((unused))) const
{
  return this->_length;
}

double trackPieceStraight::getAngle() const
{
  return 0.0;
}

double trackPieceStraight::getRadius() const
{
  return 0.0;
}

int trackPieceStraight::getInsideLane() const
{
  assert(false);
  return 0;
}

