#ifndef LAP_INFO_H_
#define LAP_INFO_H_

class lapInfo
{
public:
  lapInfo(unsigned int ticks, unsigned int milliseconds, unsigned int crashes);

  unsigned int getTicks()        const;
  unsigned int getMilliseconds() const;
  unsigned int getCrashes()      const;

private:
  unsigned int _ticks;
  unsigned int _milliseconds;
  unsigned int _crashes;
};

#endif
