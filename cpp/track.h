#ifndef TRACK_H_
#define TRACK_H_

#include <string>
#include <vector>

class trackPiece;

class track
{
public:
  track(std::string name);

  const std::string & getName() const;

  void         addPiece(double length, bool has_switch);
  void         addPiece(double angle, double radius, bool has_switch);
  trackPiece * getPiece(int index) const;

  void         addLane(double distanceFromCenter);

  int          getNextInsideLane(int pieceIndex)  const;
  double       getNextCurveAngle(int piece_index) const; /* Return the total angle of the next curve, the curve includes 1 to N number of pieces. */
  double       getDistanceBetweenLanes(int lane1, int lane2);
  unsigned int getPieceCount() const;
  unsigned int getLaneCount()  const;
  
private:
  std::string               _name;
  std::vector<trackPiece *> _pieces;
  std::vector<double>       _lane_distances; /* from center */
};

#endif
