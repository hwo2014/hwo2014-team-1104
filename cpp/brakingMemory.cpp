#include "brakingMemory.h"
#include <cassert>

brakingMemory::brakingMemory(double starting_speed, double ending_speed, double throttle)
  : _starting_speed(starting_speed),
    _ending_speed(ending_speed),
    _throttle(throttle)
{
  assert(this->_starting_speed > this->_ending_speed);
}


double brakingMemory::getThrottle() const
{
  return this->_throttle;
}


double brakingMemory::getStartingSpeed() const
{
  return this->_starting_speed;
}


double brakingMemory::getSpeedDifference() const
{
  return this->_starting_speed - this->_ending_speed;
}

