#include "trackPieceBend.h"
#include <cmath>
#include <boost/math/constants/constants.hpp>
#include <iostream>

static char * get_backtrace(void);


trackPieceBend::trackPieceBend(int index, const std::vector<double> & lanes, double angle, double radius, bool has_switch)
  : trackPiece(index, has_switch)
{
  this->_angle  = angle;
  this->_radius = radius;

  assert(lanes.size() > 0);
  for(unsigned int i = 0; i < lanes.size(); i++)
    {
      double length;

      if(angle > 0.0)
        length = (boost::math::constants::pi<double>() * (radius - lanes[i]) * angle) / 180.0;
      else
        length = (boost::math::constants::pi<double>() * (radius + lanes[i]) * std::abs(angle)) / 180.0;

      this->_lengths.push_back(length);
    }
}

bool trackPieceBend::isStraight() const
{
  return false;
}

double trackPieceBend::getLength(int lane) const
{
  if(lane < 0)
    std::cout << "lane < 0" << get_backtrace() << std::endl;

  assert(lane >= 0);
  assert(lane < static_cast<int>(this->_lengths.size()));

  return this->_lengths[lane];
}


double trackPieceBend::getAngle() const
{
  return this->_angle;
}

double trackPieceBend::getRadius() const
{
  return this->_radius;
}


int trackPieceBend::getInsideLane() const
{
  int lane;

  if(this->_angle < 0.0)
    lane = 0;
  else
    lane = this->_lengths.size() - 1;

  return lane;
}

#include <cstring>
#include <execinfo.h>

static char * get_backtrace(void)
{
  char * rv;

  rv = NULL;

  int n, len;
  void * buffer[256];
  char ** tmp;
      
  n = backtrace(buffer, 256);
  tmp = backtrace_symbols(buffer, n);
      
  len = 0;
  for(int i = 0; i < n; i++)
    len += strlen(tmp[i]) + 1;
      
  rv = static_cast<char *>(malloc(len + 1));
  assert(rv != NULL);
      
  if(rv != NULL)
    {
      strcpy(rv, "");
      for(int i = 0; i < n; i++)
        {
          strcat(rv, tmp[i]);
          strcat(rv, "\n");
        }
    }
  free(tmp);

  return rv;
}
