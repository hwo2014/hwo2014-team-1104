#include "settings.h"

settings * settings::globals = NULL;

settings::settings(std::istream & input)
  : _log_file(NULL)
{
  this->_data = jsoncons::json::parse(input);
}


void settings::dump() const
{
  std::ostream * output = this->_log_file;
  if(output == NULL)
    output = &std::cout;

  (*output) << "Settings:" << std::endl;
  for(auto it = this->_data.begin_members(); it != this->_data.end_members(); ++it)
    (*output) << "  " << it->name() << "=" << it->value().as<std::string>() << std::endl;
}


double settings::getDouble(const std::string & name) const
{
  double rv;

  if(this->_data.has_member(name))
    {
      rv = this->_data[name].as<double>();
    }
  else
    {
      std::cout << "WARNING: tried to access non-existing setting '" << name << "'" << std::endl;
      rv = 0.0;
    }

  return rv;
}


std::string * settings::getString(const std::string & name) const
{
  std::string * rv;
  
  if(this->_data.has_member(name))
    {
      rv = new std::string(this->_data[name].as<std::string>());
    }
  else
    {
      std::cout << "WARNING: tried to access non-existing setting '" << name << "'" << std::endl;
      rv = NULL;
    }

  return rv;
}


bool settings::getBool(const std::string & name) const
{
  bool rv;

  if(this->_data.has_member(name))
    {
      rv = this->_data[name].as<bool>();
    }
  else
    {
      std::cout << "WARNING: tried to access non-existing setting '" << name << "'" << std::endl;
      rv = false;
    }

  return rv;
}


int settings::getInt(const std::string & name) const
{
  int rv;

  if(this->_data.has_member(name))
    {
      rv = this->_data[name].as<int>();
    }
  else
    {
      std::cout << "WARNING: tried to access non-existing setting '" << name << "'" << std::endl;
      rv = 0;
    }

  return rv;
}



void settings::setLogFile(std::ostream & log_file)
{
  this->_log_file = &log_file;
}

std::ostream & settings::log()
{
  return *this->_log_file;
}

