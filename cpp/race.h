#ifndef RACE_H_
#define RACE_H_

#include <string>
#include <vector>

class car;
class track;

class race
{
public:
  race();

  const track & getTrack()       const;
  void          setTrack(track * track);

  void         addCar(const std::string & car_colour, double length, double width, double guide_flag_position);
  unsigned int getCarCount() const;

  void         setSessionLaps(unsigned int laps);
  unsigned int getSessionLaps() const;
  void         setSessionMaxLapTime(unsigned int milliseconds);
  unsigned int getSessionMaxLapTime() const;

  void start();
  void end();
  void lapFinish(const std::string & car_colour, unsigned int ticks, unsigned int milliseconds);
  bool isRunning()    const;
  bool isQualifying() const;

  car * getCarByColour(const std::string & colour) const;

  const std::string & getMyCarColour() const;
  void                setMyCarColour(const std::string & colour);
  car *               getMyCar() const;
  car *               getNextCar(const car * of_who) const;

  void updateCarPosition(const std::string & car_colour, double angle, int pieceIndex, double pieceDistance, int startLaneIndex, int endLaneIndex);
  void checkForCollisions();

  void turboAvailable(unsigned int duration_ticks, double factor);

  void                carCrashed(const std::string & car_colour);
  void                carCrashRecovery(const std::string & car_colour);

private:
  track *            _track;
  unsigned int       _session_laps;
  unsigned int       _session_max_lap_time;
  std::vector<car *> _cars;
  car *              _my_car;
  std::string        _my_car_colour;
  bool               _running;
  bool               _qualifying;
};

#endif
