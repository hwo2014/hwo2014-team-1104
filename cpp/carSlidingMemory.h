#ifndef CAR_SLIDING_MEMORY_H_
#define CAR_SLIDING_MEMORY_H_

class trackPiece;

class carSlidingMemory
{
public:
  carSlidingMemory(double car_angle1, double car_angle2, double speed, trackPiece * track_piece);

  double             getCarAngleChange() const;
  const trackPiece * getTrackPiece()     const;
  double             getSpeed()          const;

private:
  double       _car_angle_change;
  double       _speed;
  trackPiece * _track_piece;
};

#endif
