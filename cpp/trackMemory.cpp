#include "trackMemory.h"
#include "trackPiece.h"
#include <cmath>

trackMemory::trackMemory(trackPiece * track_piece, int lane, double entering_speed)
  : _track_piece(track_piece),
    _lane(lane),
    _entering_speed(entering_speed),
    _max_car_angle(0.0),
    _crashed(false)
{
}


void trackMemory::crash(double speed)
{
  this->_crashed        = true;
  this->_crashing_speed = speed;
}


void trackMemory::updateMaxCarAngle(double angle)
{
  if(std::abs(this->_max_car_angle) < std::abs(angle))
    this->_max_car_angle = angle;
}



int trackMemory::getLane() const
{
  return this->_lane;
}


trackPiece * trackMemory::getTrackPiece() const
{
  return this->_track_piece;
}


bool trackMemory::isCrashed() const
{
  return this->_crashed;
}


double trackMemory::getEnteringSpeed() const
{
  return this->_entering_speed;
}


double trackMemory::getCrashingSpeed() const
{
  return this->_crashing_speed;
}


double trackMemory::getMaxCarAngle() const
{
  return this->_max_car_angle;
}



void trackMemory::dump(std::ostream & output) const
{
  output << "trackMemory={";

  output << " trackPiece[" << this->_track_piece->getIndex() << "]={";
  output << "type=";
  if(this->_track_piece->isStraight())
    output << "straight";
  else
    output << "bend(" << this->_track_piece->getAngle() << ")";
  output << ",length=" << this->_track_piece->getLength(this->_lane);
  output << ",switch=" << (this->_track_piece->hasSwitch() ? "true" : "false");
  output << "}";

  output << " lane="          << this->_lane;
  output << " enteringSpeed=" << this->_entering_speed;
  output << " maxCarAngle="   << this->_max_car_angle;
  output << " crashed="       << (this->_crashed ? "true" : "false");
  output << " crashingSpeed=" << this->_crashing_speed;

  output << "}" << std::endl;
}
