#include "trackPiece.h"

trackPiece::trackPiece(int index, bool has_switch)
  : _index(index),
    _has_switch(has_switch)
{
}


int trackPiece::getIndex() const
{
  return this->_index;
}


bool trackPiece::hasSwitch() const
{
  return this->_has_switch;
}

