#ifndef BRAKINGMEMORY_H_
#define BRAKINGMEMORY_H_

class brakingMemory
{
public:
  brakingMemory(double starting_speed, double ending_speed, double throttle);

  double getThrottle()        const;
  double getStartingSpeed()   const;
  double getSpeedDifference() const;

private:
  double _starting_speed;
  double _ending_speed;
  double _throttle;
};

#endif
