#ifndef TRACKPIECESTRAIGHT_H_
#define TRACKPIECESTRAIGHT_H_

#include "trackPiece.h"

class trackPieceStraight : public trackPiece
{
public:
  trackPieceStraight(int index, double length, bool has_switch);
  
  bool   isStraight()        const override;
  int    getInsideLane()     const override;
  double getLength(int lane) const override;
  double getAngle()          const override;
  double getRadius()         const override;

private:
  double _length;
};

#endif
